syntax enable
set nocompatible              " be iMproved, required
 
set t_Co=256

colorscheme github
set number 
set backspace=indent,eol,start
let mapleader = ","			"The default leader is \, but a comma is much better"


set tabstop=4
set shiftwidth=4

set guifont=Fira_Code:h12
set guioptions-=l
set guioptions-=L
set guioptions-=r
set guioptions-=R
set guioptions-=e

set autowriteall

" set foldcolumn=0
" hi foldcolumn guibg=bg
" No split borders 
hi vertsplit guifg=bg guibg=bg

"--Highlighting Search--"
set hlsearch
set incsearch



" greplace
set grepprg=ag
let g:grep_cmd_opts = '--line-numbers --noheading'


"--Mappings--"
"--imap--"
"--, is a namespace--"
nmap <Leader>ev :tabedit ~/.vimrc<cr>
nmap <Leader><space> :nohlsearch<cr>
nnoremap <leader>p :set pastetoggle<CR>

nmap <D-1> :NERDTreeToggle<cr>
" nmap <-R> :CtrlPBufTag<CR>
nmap <D-e> :CtrlPMRUFiles<CR>
"--Split Managment--"
"C-W | Zoom C-W = UnZoom"
set splitbelow
set splitright
nmap <C-J> <C-W><C-J>
nmap <C-K> <C-W><C-K>
nmap <C-L> <C-W><C-L>
nmap <C-H> <C-W><C-H>

nmap <D-p> :CtrlP<cr>


nmap <Leader>f :tag<space>
let g:ctrlp_custom_ignore = 'node_modules\DS_STORE\|git'
let g:ctrlp_match_window = 'top,order:ttb,min:1,max:30,results:30'

let NERDTreeHijackNetrw = 0


"--Auto-Command--"
augroup autosourcing
	autocmd!
	autocmd BufWritePost .vimrc source %
augroup END


so ~/.vim/plugins.vim
so ~/.vim/phpScripts.vim

set backupdir=~/.vim/backup//
set directory=~/.vim/swap//
set undodir=~/.vim/undo//

