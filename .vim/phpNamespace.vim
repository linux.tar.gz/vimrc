function! IPhpInsertUse()
	call PhpInsertUse()
	call feedkeys('a', 'n')
endfunction

function! IPhpExpandClass()
	call PhpExpandClass()
	call feedkeys('a', 'n')
endfunction

vmap <Leader>su ! awk '{ print length(), $0 \| "sort -n \| cut -d\\  -f2-" }'<CR>
