so ~/.vim/phpSyntaxScript.vim
so ~/.vim/phpNamespace.vim

augroup phpFuntions
	autocmd!
	autocmd FileType php call PhpSyntaxOverride()
	autocmd Filetype php inoremap <Leader>n <Esc>:call IPhpInsertUse()<CR>
	autocmd Filetype php noremap <Leader>n :call IPhpInsertUse()<CR>
	autocmd Filetype php inoremap <Leader>nf <Esc>:call IPhpExpandClass()<CR>
	autocmd Filetype php noremap <Leader>nf :call IPhpExpandClass()<CR>
augroup END
