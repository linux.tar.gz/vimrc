function! PhpSyntaxOverride() 
	"Put snippet overrides in this function
	hi! link phpDocsTags phpDefine
	hi! link phpDocParam phpType
endfunction

hi phpUseNamespaceSeperator guifg=#808080 guibg=NONE gui=NONE
hi phpClassNamespaceSeperator guifg=#808080 guibg=NONE gui=NONE

augroup phpSyntaxOverride
	autocmd!
	autocmd FileType php call PhpSyntaxOverride()
augroup END
